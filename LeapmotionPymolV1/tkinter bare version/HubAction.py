"""actions for in the ActionHub menu"""
import sys
import time
from pymol import cmd, util
from pymol.cgo import *
from LeapListener import Leap 
from Leap import Matrix, Vector
from LeapMenuStructure import Action
#classes
class ZoomAction(Action):
    """you can zoom in by extending both hands and rotating your wrists"""
    def test_for_toggle(self, frame):
        return len(frame.hands) == 2
    
    def run(self, frame):
        #get view from pymol
        view = list(cmd.get_view())
        
        #get hands from frames
        hands = frame.hands
        
        #calculate new view
        view = self.get_zoomed_view(view, hands)
        #apply new view
        cmd.set_view(view)
        pass
    
    def get_zoomed_view(self, view, hands):
        leftmost = hands.leftmost
        rightmost = hands.rightmost
        hand_center_l = leftmost.palm_position
        hand_center_r = rightmost.palm_position
        roll = leftmost.palm_normal.roll
        s = hand_center_l.distance_to(hand_center_r)/200
        if roll <= 1:
            view[11] += s
            view[15] -= s
            view[16] -= s
        elif roll >= 2.3 :
            view[11] -= s
            view[15] += s
            view[16] += s
        return view

class RotateAction(Action):
    """rotates the molecule based on fist rotation"""
    def test_for_toggle(self, frame):
            return self.parent.prev_frame and frame.rotation_probability(self.parent.prev_frame) > 0.3
        
    def run(self, frame):
        view = list(cmd.get_view())
        m = frame.rotation_matrix(self.parent.prev_frame)
        m *= Matrix(Vector(*view[0:3]), Vector(*view[3:6]), Vector(*view[6:9]))
        view[:9] = m.to_array_3x3()

        cmd.set_view(view)
        return


class SpinAction(Action):
    """spins the molecule when a swipe is detected"""
    rotating = False
    
    def test_for_toggle(self, frame):
        result = False
        for gesture in frame.gestures():
            if gesture.type is Leap.Gesture.TYPE_SWIPE:
                result = True
                break
        return result
    
    def run(self, frame):
        print(">>> ",self.rotating)
        if self.rotating:
            self.rotating = False
            cmd.mset("1 x1")
            print("stopping rotating")
        else:
            self.rotating = True
            cmd.mset("1 x360")
            util.mroll()
            cmd.mplay()
        time.sleep(1)
        return
        
