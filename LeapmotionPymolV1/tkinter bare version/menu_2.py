#!/usr/bin/env python2.7
"""script should only be run from within pymol"""

from __future__ import print_function

import sys
import time
import threading
import pymol_commands
import scenes
import HubAction
import LeapMenuStructure as LMS

from LeapListener import LeapListener

from Tkinter import *

#globals

FRAME_DELAY = 0

#the tkinter root that holds the GUI and the LeapMotionListener object
LEAP_INTERFACE = None
INTERFACE_THREADING_LOCK = threading.Lock()

#classes
class MenuThread(threading.Thread):
    """a tdef __del__(self):
        self.controller.remove_listener(self)
        super(PymolListener, self).__del__()hread for running tkinter alongside other applications"""
    
    def run(self):
        """function will run in its own thread when self.start() is called"""
        global LEAP_INTERFACE
        self.interface = LEAP_INTERFACE = MainInterface()
        #release the IINTEFACE_THREADING_LOCK
        INTERFACE_THREADING_LOCK.release()
        #start the mainloop. makes the menu window stay on screen
        self.interface.mainloop()
        return



class MainInterface(Tk):
    
    def __init__(self):
        """initialise self"""

        Tk.__init__(self)
        #control what happens when the menu is closed
        self.protocol("WM_DELETE_WINDOW", self.quit)
        
        #title
        self.title('Leapmenu fingerselect')
        self.application = self
        
        #create "listening for gesture" indicator
        self.indicator = Label(self, text="main interface", heigh=3)
        self.indicator.pack()
        
        #create the menu items from Leapmenustructure
        #must include self.root_menu
        self.root_menu = LMS.SwipeMenu(self, label="root menu", descr="none")
        self.root_menu.option_listening = 1
        
        # define global menus
        menu_1 = LMS.ListMenu(self.root_menu, label="structures")
        menu_2 = LMS.ListMenu(self.root_menu, label="ribosome_demo")
        menu_3 = LMS.ActionHub(self.root_menu, label="action hub")
        #command=lambda: presets.load_preset(['"4d5f-pdb-bundle3" in cmd.get_object_list() or (cmd.delete("all") or cmd.do("@ribosome.pml"))'])
        # define actions under menus
        
        # menu 1
        action_1_1 = LMS.ListMenuAction(menu_1, command=lambda: pymol_commands.exec_strings(["cmd.mstop()", 'cmd.disable("all")','cmd.enable("1bsd")', 'cmd.orient("1bsd")']), label="Load 1bsd")
        action_1_2 = LMS.ListMenuAction(menu_1, command=lambda: pymol_commands.exec_strings(["cmd.mstop()", 'cmd.disable("all")','cmd.enable("1bsn")', 'cmd.orient("1bsn")']), label="Load 1bsn")
        action_1_3 = LMS.ListMenuAction(menu_1, command=lambda: pymol_commands.exec_strings(["cmd.mstop()", 'cmd.disable("all")','cmd.enable("3WI6")', 'cmd.orient("3WI6")']), label="Load 3WI6")
        # menu 2
        action_2_1 = LMS.ListMenuAction(menu_2, command=lambda: pymol_commands.exec_strings(["cmd.mstop()", "cmd.disable('all')" , "cmd.enable('4v5f*')", "cmd.orient('4v5f*')"]), label="Load view 1")
        action_2_2 = LMS.ListMenuAction(menu_2, command=lambda: pymol_commands.exec_strings(["cmd.mstop()", "cmd.disable('all')" , "cmd.enable('4v5d*')", "cmd.orient('4v5f*')"]), label="Load view 2")
        action_2_3 = LMS.ListMenuAction(menu_2, command=lambda: pymol_commands.exec_strings(["cmd.mstop()", "cmd.disable('all')" , "cmd.enable('4v5g*')", "cmd.orient('4v5f*')"]), label="Load view 3")
        action_2_4 = LMS.ListMenuAction(menu_2, command=lambda: pymol_commands.exec_strings(["cmd.mplay()", "cmd.disable('all')", "cmd.enable('4v5f*')", "cmd.orient('4v5f*')"]), label="Movie")
        # menu 3
        action_3_1 = HubAction.ZoomAction(menu_3)
        action_3_2 = HubAction.RotateAction(menu_3)
        #action_3_3 = HubAction.SpinAction(menu_3)


        print("finished MainInterface init")
        return
        
    def add_option(self, option):
        pass
        return
    
    def add(self, *args, **kwargs):
        pass
        return


    def on_frame (self, frame):
        """handle the frame and redirect to other menus"""
        # run the frame through the menus
        self.root_menu.run(frame)
        time.sleep(FRAME_DELAY)
        return
    
    def quit(self):
        """stopt the program"""
        #stop the leap listener
        self.listener.stop_event_loop()
        
        #destroy the menu / Tk instance
        self.destroy()
        print("stopped MainInterface")
        return


#functions

def clear_screen():
    """clears the screen of the terminal (though not the pymol console)"""
    if sys.platform == 'win32' or sys.platform == 'cygwin':
        os.system('cls')
    else:
        os.system('clear')
    return

def main():
    #~ load a pdb file to pymol
    #~ cmd.fetch("1bsd")
    # load in pymol files
    scenes.scene4()
    scenes.scene1()
    scenes.scene2()
    scenes.scene3()
    
    
    
    
    menu = MenuThread()
    
    INTERFACE_THREADING_LOCK.acquire()
    menu.start() #will unlock INTERFACE_THREADIG_LOCK
    
    #lock makes shure menu.interface has finished initialising
    with INTERFACE_THREADING_LOCK:
        listener = None
        listener = LeapListener(menu.interface)
        listener.daemon
        #start processing frames
        listener.start()

    return 


if __name__ == "pymol" or __name__ == "__main__":
    main()
