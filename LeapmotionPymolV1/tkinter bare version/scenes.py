#scene1, cartoon
from pymol import cmd

def scene1(pdb="3WI6"):
    if pdb not in cmd.get_object_list():
        #cmd.delete("all")
        cmd.fetch(pdb)
        
    cmd.hide()
    cmd.show("cartoon")
    cmd.color("white", '3WI6')
    cmd.color("red", "3WI6 and ss h")
    cmd.color("yellow", "3WI6 and ss s")
    cmd.set("cartoon_fancy_helices", "on")
    cmd.orient()
#scene2, sticks and spheres
from pymol import cmd

def scene2(pdb="1bsd"):
    if pdb not in cmd.get_object_list():
        #cmd.delete("all")
        cmd.fetch(pdb)
        
    cmd.hide()
    cmd.show("spheres")
    cmd.show("sticks")
    cmd.set("sphere_scale", "0.3")
    cmd.color("white", '1bsd')
    cmd.color("red", "1bsd and ss h")
    cmd.color("yellow", "1bsd and ss s")
    cmd.set("cartoon_fancy_helices", "on")
    cmd.orient()
#scene3, cartoon+sticks+spheres
from pymol import cmd

def scene3(pdb="1bsn"):
    if pdb not in cmd.get_object_list():
        #cmd.delete("all")
        cmd.fetch(pdb)
        
    cmd.hide()
    cmd.show("cartoon")
    cmd.show("sticks")
    cmd.show("spheres")
    cmd.set("sphere_scale", "0.3")
    cmd.color("white", '1bsn')
    cmd.color("red", "1bsn and ss h")
    cmd.color("yellow", "1bsn and ss s")
    cmd.set("cartoon_fancy_helices", "on")
    cmd.orient()
#scene4, ribosome
from pymol import cmd

def scene4():
    bool("4d5f-pdb-bundle3" in cmd.get_object_list() or cmd.do("@ribosome.pml"))
