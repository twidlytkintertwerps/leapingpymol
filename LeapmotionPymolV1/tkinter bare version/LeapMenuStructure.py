
"""
   module for holding Leap menu classes, which are all subclasses of Option.
   An handle an incomming frame from the leap motion 
"""

#imports
from __future__ import print_function
import time
import Tkinter
import ttk
from LeapListener import Leap

# constants

#time to wait afer a gesture is made
GESTURE_TIMEOUT = 1.5

#classes
class Tab(Tkinter.Frame):
    """Tab(frame) for easy insertion into a ttk.Notebook"""
    def __init__(self, parent, title=""):
        """initialise self
           args:
           -notebook: the notebook into which the tab should be inserted.
           -title: the text that is displayed 
        """
        self.parent = parent
        self.tab_title = title
        
        Tkinter.Frame.__init__(self, parent)
        self.pack(expand=1, fill=Tkinter.BOTH)

        #add self to notebook
        self.parent.add(self, text=self.tab_title)
        return
        
    def scroll_to(self):
        """let the parent notebook navigate to this tab"""
        self.parent.select(self)
        return


class Option():
    """pickable object, handles incomming frame and performs work based on it."""
    def __init__(self, parent, **kwargs):
        """
           initialise self.
           args:
           -parent:     the parent-option.
           -**kwargs:   keyword-arguments:
                        -label: the "title" of the option (str)
                        -descr: description of what self does.
                        -gesture: description of the gesture to be made.
                        -active:bool. if True, self wil be run by default,
                                as soon as parent receives a frame.
        """
        self.prev_frame = None
        
        #save kwargs
        #~ (self, parent, label="option_name", descr="toggle_gesture", active=False)
        
        self.label = kwargs.pop("label", "option_name(not defined yet)")
        self.description = kwargs.pop("descr", "toggle_gesture(not defined yet)")
        self.gesture = kwargs.pop("gesture", "wave hands vigorously")
        self.active = kwargs.pop("active", False)
        
        if kwargs:
            raise TypeError(type(self).__name__ + ".__init__() got unexpected keyword arguments:", *kwargs)
        
        #install self in parent menu
        #should be implemented in TkinterMenuStructure.py
        self.parent = parent
        
        self.application = self.parent.application
        self.parent.add_option(self)
        
        return

    #__str__ should not be defined. causes trouble with ttk.notebook.__init__

    def test_for_stop(self, frame, amount_of_fingers=9):
        """test if frame contains a 'stop' gesture. returns true if so"""
        #~ print("testing for stop")
        return len(frame.hands) == 2 and len(frame.fingers.extended()) == amount_of_fingers
        
    def test_for_start(self, frame):
        return len(frame.hands) == 2 and len(frame.fingers.extended()) == 10

    def is_active(self):
        return self.active

    def activate(self):
        self.active = True
        return

    def deactivate(self, delay=0):
        self.active = False
        self.prev_frame = None
        time.sleep(delay)
        return
    
    def is_active(self):
        return self.active

    def run(self, frame):
        """
        hook. Should be overridden by subclass
        analises incomming frame and then decide what to do
        """
        #self.is_active = True
        #work
        return


class Action(Option):
    """option that performs work in PyMol when run() is called"""
    def __init__(self, parent, toggle_test=None, command=None, **kwargs):
        """
           initialise self
           args:
           -parent: parent-option.
           toggle_test: reference to function (orlambda) that will test a frame
                        and determines if self.run should be called. must only return True or False.
           -command:    reference to function (or lambda) that contains the work (in pymol) 
                        that should be done when this action is selected. 
           -**kwargs:   see 'Option'.
        """
        Option.__init__(self, parent, **kwargs)
        
        self.toggle_test = toggle_test
        #function to be called when self.run is called
        self.command=command
        return
    
    
    def test_for_toggle(self, frame):#should be outsourced to a subclass
            """
                tests frame  for a certain gesture
                returns true if right gesture was made, false otherwise
            """
            if self.toggle_test is not None:
                result = self.toggle_test(frame, *self.toggle_args)
            else:
                result = False
            return result

    def run(self, frame):
        """hook can be overridden. if not overridden, will execute self.command if there is one """
        if self.command is not None:
            self.command()
            
        #deactivate self, so that the command only gets executed once.
        self.deactivate()
        return

class Menu(Option):
    """pickable object(Option) that holds other options"""
    def __init__(self, parent, **kwargs):
        #initialise self as option
        Option.__init__(self, parent, **kwargs)
        
        #define properties and states of self
        self.options = []
        self.option_listening = False
        return
    
    def add_option(self, option):
        """add an option object to self.options"""
        self.options.append(option)
        return
    
    def add_options(self, *args):
        for option in args:
            self.add_option(option)
        return
    
    #__str__ should not be defined. causes trouble with ttk.notebook.__init__
        
    def run(self, frame):
        """
        hook. gets called when menu(self) is activated by self.parent.
        should handle incomming frame and redirect to other options
        """
        if self.option_listening:
            self.application.indicator.config(bg="green")
        else:
            self.application.indicator.config(bg="orange")
        return


class SwipeMenu(Menu, Tab, ttk.Notebook):
    """menu for holding other menus"""
    def __init__(self, parent, **kwargs):
        Menu.__init__(self, parent, **kwargs)
        
        ttk.Notebook.__init__(self, parent)
        self.pack(expand=1, fill=Tkinter.BOTH)
        
        #add self to notebook
        self.parent.add(self, text=self.label)
        
        self.current_option_index = 0
        print("finished SwipeMenu init")
        return
    
    def select_next_option(self):
        """-up self.current_option_index by one (wrap to 0 if needed)
           -navigate gui to next tab in self
        """
        print("selecting next option")
        
        #increment or wrap self.current_option_index
        if self.current_option_index < len(self.options) - 1:
            self.current_option_index +=1
        else:
            self.current_option_index = 0
        
        #navigate gui to next tab
        self.options[self.current_option_index].scroll_to()
        return
    
    def select_previous_option(self):
        print("selecting previous option")
        """-lower self.current_option_index by one (wrap to end if needed)
           -navigate gui to previous tab in self
        """
        #decrement or wrap self.current_option_index
        if self.current_option_index > 0:
            self.current_option_index -= 1
        else:
            self.current_option_index = len(self.options) - 1
        
        #navigate gui to previous tab
        self.options[self.current_option_index].scroll_to()
        return
        
    def activate_current_option(self):
        index = self.current_option_index
        #disable all other tabs
        for n in range(len(self.options)):
            if n != index:
                self.tab(n, state="disabled")
        
        #activate proper option
        self.options[index].activate()
        return
    
    def run(self, frame):
        # check if there's an active option in the menu
        # if option is active, redirect frame to it.
        for option in self.options:
            #each option is a tab
            if option.is_active():
                #no need to navigate the gui to it, should allready be done.
                #send frame to child option
                self.prev_frame = None
                option.run(frame)
                break
        else:
        #if no active option was found
            #toggle application.indicator
            Menu.run(self, frame)
            
            #enable all tabs (sould only be ran the first time the menu gets focus)
            if self.prev_frame is None:
                print("enabling tabs")
                for n in range(len(self.options)):
                    self.tab(n, state="normal")
            #test for start gesture
            if self.test_for_start(frame):
                print("start gesture selected")
                #activate currently selected option
                self.activate_current_option()
                wait()
            elif self.test_for_stop(frame):
                self.deactivate()
                wait(times=0.3)
            else:
            #if no option was run:
                #detect swipes, stop after first swipe in frame
                for gesture in frame.gestures():
                    if gesture.type is Leap.Gesture.TYPE_SWIPE:
                        swipe = Leap.SwipeGesture(gesture)
                        swipe_direction = "right" if swipe.direction[0] > 0 else "left"
                        print("detected swipe!")
                        print(swipe_direction)
                        
                        #select next or previous option in self
                        if swipe_direction == "right":
                            self.select_next_option()
                        elif swipe_direction == "left":
                            self.select_previous_option()
                        else:
                            print("you jus swiped in no direction !?")
                        
                        #give user time to act
                        wait(times=0.3)
                        #stop after first swipe (there may be several swipes in same frame)
                        self.prev_frame = frame
                        break
                else:
                    #if none of the above triggered something
                    self.prev_frame = frame
                    #wait for next frame
                    pass
            
        return
    

class ListMenu(Menu, Tab):
    '''
    A menu object that listens to the amount of fingers extended
    attributes = 
    self.opt_listening
    '''
    def __init__(self, parent, **kwargs):
        Menu.__init__(self, parent, **kwargs)
        Tab.__init__(self, parent, title=self.label)
        return
    
    def add_option(self, action):
        """add option object to self.options and set its descripion
           overrides and extends Menu.add_option
        """
        Menu.add_option(self, action)
        action.gesture = "Show " + str(len(self.options)) + "fingers"
        return
    
    def run(self, frame):
        """
           handle incomming frame and redirect it to other options,
           depending on the gestures in it
        """
        # check if there's an active option in the menu
        # if option is active, redirect frame to it.
        for option in self.options:
            if option.is_active():
                option.run(frame)
                break #dance
        else:
        #if no active option was found
            #toggle application.indicator
            Menu.run(self, frame)
            
            if self.option_listening == True:
                #test if a suitable amount of fingers is shown
                shown_fingers = len(frame.fingers.extended())
                print("detected", shown_fingers, "extended fingers")
                if  0 < shown_fingers <= len(self.options):
                    self.options[shown_fingers - 1].activate()
                    self.option_listening = False
                    
                # check for stop signal if none of the above options was toggled
                elif self.test_for_stop(frame):
                    print("stop detected")
                    self.deactivate()
                    self.option_listening = False
                    
            #if self.option_listening == false
            # do nothing until 2 hands and 10 fingers are shown
            elif self.test_for_start(frame):
                self.option_listening = True
                #toggle application.indicator
                Menu.run(self, frame)
                # give the user time to act
                wait()
        self.prev_frame = frame
        return
        
class ListMenuAction(Action, Tkinter.Frame):
    def __init__(self, parent, command=None, **kwargs):
        """initialise self"""
        #initialise self as Action
        Action.__init__(self, parent, command=command, **kwargs)
        #initialise self as Frame
        Tkinter.Frame.__init__(self, parent)
        
        #draw self in parent
        self.draw()
        return
        
        
    def draw(self):
        """draw tkinter widgets on self.parent"""
        #apply styling
        self.config(bd=4)
        
        self.pack(side=Tkinter.LEFT, fill=Tkinter.Y)
        
        self.description_label = Tkinter.Label(self, text=self.description)
        self.description_label.pack()
        
        self.button = Tkinter.Button(self, text=self.label, command=self.run, activebackground="red")
        self.button.pack(side=Tkinter.TOP)
        
        self.gesture_label = Tkinter.Label(self, text=self.gesture)
        self.gesture_label.pack(side=Tkinter.TOP)
    
        return
    
    def run(self, frame=None):
        """sun execute self.command and flash the button"""
        #flash self.button
        self.button.flash()
        #run the command that is defined thru the superclass
        Action.run(self, frame)
        return

class ActionHub(Menu, Tab):
    def __init__(self, parent, **kwargs):
        Menu.__init__(self, parent, **kwargs)
        Tab.__init__(self, parent, title=self.label)
        return
        
    def run(self, frame):
        #test each option in self for a toggle
        if self.test_for_stop(frame, amount_of_fingers=0):
            print("detected stop in action hub")
            self.deactivate()
        else:
            print("testing options in action hub")
            for action in self.options:
                if action.test_for_toggle(frame):
                    action.run(frame)
        self.prev_frame = frame
        return

#functions

def wait(times=1):
    print("*"*10, "sleeping", "*"*10)
    time.sleep(GESTURE_TIMEOUT * times)
    print("done sleeping")
    return

def check_fingers(frame, finger_amount):
    '''
    check amount of given fingers extended, return true if
    amount of fingers extended is right.
    '''
    #~ print("testing for", str(finger_amount), "fingers")
    return True if len(frame.fingers.extended()) == finger_amount and len(frame.hands) > 0 else False
