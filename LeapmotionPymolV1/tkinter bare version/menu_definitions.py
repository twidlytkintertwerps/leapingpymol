        Tk.__init__(self)
        #control what happens when the menu is closed
        self.protocol("WM_DELETE_WINDOW", self.quit)
        
        #title
        self.title('Leapmenu fingerselect')
        self.application = self
        
        #create "listening for gesture" indicator
        self.indicator = Label(self, text="main interface", heigh=3)
        self.indicator.pack()
        
        #create the menu items from Leapmenustructure
        #must include self.root_menu
        self.root_menu = LMS.SwipeMenu(self, label="root menu", descr="none")
        self.root_menu.option_listening = 1
        
        # define global menus
        menu_1 = LMS.ListMenu(self.root_menu, label="structures")
        menu_2 = LMS.ListMenu(self.root_menu, label="ribosome_demo", command=lambda: presets.load_preset(['delete all', 'do @ribosome.pml']))
        #menu_2 = LMS.ActionHub(self.root_menu, label="action hub")
        
        # define actions under menus
        
        # menu 1
        action_1_1 = LMS.ListMenuAction(menu_1, command=lambda: presets.load_preset("scene1.txt"), label="Load preset 1")
        action_1_2 = LMS.ListMenuAction(menu_1, command=lambda: presets.load_preset("scene2.txt"), label="Load preset 2")
        action_1_3 = LMS.ListMenuAction(menu_1, command=lambda: presets.load_preset("scene3.txt"), label="Load preset 3")
        # menu 2
        action_2_1 = LMS.ListMenuAction(menu_2, command=lambda: presets.load_preset(["mstop", "enable 4v5f*" , "disable 4v5d*" , "disable 4v5g*"]), label="Load view 1")
        action_2_2 = LMS.ListMenuAction(menu_2, command=lambda: presets.load_preset(["mstop", "disable 4v5f*" , "enable 4v5d*" , "disable 4v5g*"]), label="Load view 2")
        action_2_3 = LMS.ListMenuAction(menu_2, command=lambda: presets.load_preset(["mstop", "disable 4v5f*" , "disable 4v5d*" , "enable 4v5g*"]), label="Load view 3")
        action_2_4 = LMS.ListMenuAction(menu_2, command=lambda: presets.load_preset(["mstart", "enable 4v5f*" , "disable 4v5d*" , "disable 4v5g*"]), label="Movie")
        #action_3_1 = HubAction.ZoomAction(menu_3)
        #action_3_2 = HubAction.RotateAction(menu_3)
        #action_3_3 = HubAction.SpinAction(menu_3)


        print("finished MainInterface init")
