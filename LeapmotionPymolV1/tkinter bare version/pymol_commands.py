#!/usr/bin/env python2.7
'''
docstring
'''
# imports
import sys
import re
from pymol import cmd
# constants
# classes
# functions
def exec_strings(listy):
    # check if instance is string and make it a readlines list
    if isinstance(listy, list):
        for command in listy:
            exec command
        
    elif not isinstance(opened, list):
        print('commands are defined correctly, exiting...')
    return


def change_color(color):
    cmd.color(color)


def change_helix_color(color):
    cmd.color(color, 'ss h')


def change_sheet_color(color):
    cmd.color(color, 'ss s')

def toggle_representation(representation, state):
    if state:
        stringy = 'cmd.hide('+representation+')'
        state = False
    else:
        stringy = 'cmd.show("'+representation+'")'
        state = True
    exec stringy
    return state
        

def main(argv=None):
    '''
    initiates program
    '''
    if argv is None:
        argv = sys.argv
    # work
    change_sheet_color('red')
    load_preset('scene2.txt')
    cart = False
    cart = toggle_representation('cart', cart)
    return 0

# initiate
if __name__ == "pymol" or __name__ == "__main__":
    sys.exit(main())
