import glob


# Load in the pdb files
for pdb in glob.glob("*bundle[34].pdb"): cmd.load(pdb)


# Align 4v5d with the other two
align 4v5d-pdb-bundle3, 4v5f-pdb-bundle3
matrix_copy 4v5d-pdb-bundle3, 4v5d-pdb-bundle4


orient
hide
show cartoon
set cartoon_ring_mode, 3


# White for protein
color white


# Grey for RNA
color grey70, byres e. p


# tRNA
color hotpink, 4v5f-pdb-bundle3 and c. V+W
color hotpink, 4v5g-pdb-bundle3 and c. V+W+Y
color hotpink, 4v5d-pdb-bundle3 and c. V+W+Y


# mRNA
color chartreuse, 4v5d-pdb-bundle3 and c. X
color chartreuse, 4v5f-pdb-bundle3 and c. X
color chartreuse, 4v5g-pdb-bundle3 and c. X


# Close-up on tRNA 
set_view (\
   -0.558972359,    0.074776784,   -0.825802028,\
    0.583282351,    0.743315637,   -0.327515364,\
    0.589335382,   -0.664756835,   -0.459109515,\
    2.000000000,   40.000000000, -140.400131226,\
  112.201690674, -150.282562256,  145.545547485,\
   72.047866821,  208.752288818,  -20.000000000 )
view 9, store


# Protein pushing stuff through
color gold, 4v5f-pdb-bundle3 and c. Y


# Carrier protein placing new tRNA
color brightorange, 4v5g-pdb-bundle3 and c. Z


# Protein being synthesized
color marine, 4v5g-pdb-bundle3 and c. a
color marine, 4v5f-pdb-bundle3 and c. Z
color marine, 4v5d-pdb-bundle3 and c. Z


# Color the phenylalanine that is bound to tRNA (PHA)
show spheres, r. PHA
util.cbay r. PHA


# Another close up of tRNA
set_view (\
    -0.739631295,    0.203862771,   -0.641390383,\
     0.554865837,    0.724050164,   -0.409722686,\
     0.380869687,   -0.658930898,   -0.648651659,\
     5.000000000,   -4.000000000,  -29.133359909,\
   112.344810486, -169.997711182,  181.820770264,\
    18.555110931,   39.711635590,  -40.000000000 )
view 1, store

# change stupid color
color grey70, 4v5g*3 and c. A
# orient stuff
orient
# movie
mset 1 x3
cmd.mdo(1,"disable 4v5d*; enable 4v5f*")
cmd.mdo(2,"disable 4v5f*; enable 4v5g*")
cmd.mdo(3,"disable 4v5g*; enable 4v5d*")
set movie_fps, 1.5
hide sticks
hide spheres
