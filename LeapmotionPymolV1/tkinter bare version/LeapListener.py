"""module for holding the LeapListener class"""

#imports
import sys
import threading

sys.path.append('/homes/ccpeters/th5_fun/pymol/LeapSDK/lib')
import Leap

#classes

class LeapListener(threading.Thread):
    """Listener that will run in a seperate thread and will poll the leapmotion sensor for frames"""
    def __init__(self, interface):
        """
           initialise self
           args:
           - interface: an object that should at least have an on_frame methot that only takes a frame.
        """
        #initialise self as thread
        threading.Thread.__init__(self)
        
        #connect to the Leap controller (sensor)
        self.controller = Leap.Controller()
        
        #save attributes
        self.interface = interface
        self.prev_frame = None
        self.listening = False
        
        #enable automatic detection of gestures
        self.enable_gestures()
        
        #'install' self in self.interface
        self.interface.listener = self
        print("finished LeapListener init")
        return
    
    def enable_gestures(self):
        """enable automatic gesture detection (and inclusion in frames) by self.controller"""
        controller = self.controller
        #***add gestures here***
        
        #enable and configure the swipe gesture
        controller.enable_gesture(Leap.Gesture.TYPE_SWIPE)
        controller.config.set("Gesture.Swipe.MinLength", 500)
        controller.config.set("Gesture.Swipe.MinVelocity", 1000)
        controller.config.save()
        
        return

    def get_new_frame(self):
        '''
        keep getting new frame from the controller until it's a new one.
        '''
        
        # always request latest frame from controller
        current_frame = self.controller.frame()
        #if this is the fist time the function is called
        if self.prev_frame is None:
            self.prev_frame = current_frame
        # if previous frame is same as current frame, wait for a new frame and return it.
        while current_frame.id == self.prev_frame.id:
            if not self.listening:
                break
            else:
                 #get a (new?) frame
                current_frame = self.controller.frame()
        else:
            # if there's a new frame
            self.prev_frame = current_frame
            return current_frame
            
    
    def stop_event_loop(self):
        """stop polling for frames"""
        print("stopping event loop")
        self.listening = False
        return

    
    def run(self):
        '''.
        start the thread, startpolling for frames
        get a new frame, pass it to self.interface
        '''
        self.listening = True
        while self.listening == True:
            #test if a sensor is connected
            if not self.controller.is_connected:
                self.interface.indicator.config(bg="red")
            else:
                pass
                
            #get a new frame
            frame = self.get_new_frame()

            #pass new frame to interface.onframe, which will do things with it
            if frame is not None:
                self.interface.on_frame(frame)
            else:
                pass
        return
